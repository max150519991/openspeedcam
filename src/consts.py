from os import getenv

CATEGORY_CODES = {
    '1': 'статическая камера',
    '2': 'камера, встроенная в светофор',
    '3': 'камера, контролирующая проезд на красный свет',
    '4': 'парная камера (Автодория)',
    '5': 'полицейская засада',
    '100': 'пешеходный переход',
    '101': 'ограничение скорости',
    '102': 'лежачий полицейский',
    '103': 'плохая дорога',
    '104': 'опасное изменение направления движения',
    '105': 'опасный перекрёсток',
    '106': 'другая опасность'
}  # Словарь соответствий типов опасностей

DIR_TYPE = {
    '0': ' все направления (360)',
    '1': 'в определенном направлении (смотреть поле DIRECTION )',
    '2': 'два направления (DIRECTION + обратное направление)'
}  # Словарь соответствий направлений действий камеры

# URL-адрес сайта
URL = 'https://openspeedcam.net/api/export'

# Заголовки
HEADERS = {
    'authority': 'openspeedcam.net',
    'accept': 'application/json, text/plain, */*',
    'content-type': 'application/json;charset=UTF-8',
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36'
}

# Куки
COOKIES = {
    'lang': 'ru',
    'map-type': 'roadmap',
    'avtomap.sid': getenv('avtomap.sid')
}

# Данные
DATA = {
    'format': 'Navitel',
    'rating': 1,
    'country': ['RU'],
    'types': ['static_cam', 'control_cam_red', 'speed_cam', 'traffic_control_ot', 'video_control', 'dummy',
              'mobile_ambush', 'stationary_porst_dps', 'railroad_crossing', 'begin_village', 'end_village',
              'crosswalk', 'cautiouslychildren', 'speed_limit', 'rec_policeman', 'bad_road', 'dang_turn',
              'dang_intersection', 'dang_other', 'overtaking_proh'],
    'selectMiddlePoints': False,
    'withIdPoint': True,
    'extended': False,
    'saveToUserExport': False,
    'userExportName': '',
    'confirmed': '2022-06-05',
    'selectOnlyConfirmed': True
}

# Интервал, через который осуществляется парсинг данных (количество секунд в сутках)
INTERVAL_TO_PARSE = 86_400
