import psycopg2  # Библиотека для работы с PostgreSQL
from psycopg2 import extras
from time import sleep
import logging

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(name)s - %(message)s',
    level=logging.DEBUG,
    datefmt='%Y-%m-%d %H:%M:%S'
)
logger = logging.getLogger('DB')


class DB:
    __slots__ = ('__host', '__port', '__user', '__password', '__db_name',
                 '__connection', '__timeout')  # Кортеж атрибутов объекта

    def __init__(self, host, port, user, password, db_name):
        """ Конструктор """

        self.__host = host  # Хост для соединения с БД
        self.__port = port # Порт для соединения с БД
        self.__user = user  # Имя пользователя
        self.__password = password  # Пароль
        self.__db_name = db_name  # Название БД
        self.__connection = None  # Соединение
        self.__timeout = 60  # Тайм-аут подключения к БД

    def connection_with_db(self):
        """ Метод для установления соединения с БД """

        timeout = self.__timeout

        while timeout != 0:
            try:
                self.__connection = psycopg2.connect(
                    host=self.__host,
                    port=int(self.__port),
                    user=self.__user,
                    password=self.__password,
                    database=self.__db_name
                )
            except Exception as ex:
                logger.error(f'Failed connect to database {self.__db_name}: {ex}')
                timeout -= 1
                logger.info(f'Tries left: {timeout}')
                sleep(1)
            else:
                logger.debug('Database connection established')
                self.__connection.autocommit = True
                break

        else:
            raise Exception(f'Failed connect to database {self.__db_name}')

    def query_execute(self, query: str, params: tuple=(), fetchone=False, fetchall=False, ext=False):
        """
        Метод, выполняющий запрос к БД

        :param ext: Булева переменная, если её значение True - это значит что обработать необходимо много данных
        :param fetchall: Булева переменная, если её значение True будут выданы все записи из БД
        :param fetchone: Булева переменная, если её значение True будет выдан одна запись из БД
        :param params: Кортеж с данными
        :param query: запрос к БД
        """

        if not self.__connection:
            self.connection_with_db()

        with self.__connection.cursor() as cur:
            try:
                if ext:
                    extras.execute_batch(cur, query, params)
                else:
                    cur.execute(query, params)

                if fetchone:
                    res = cur.fetchone()
                    return res

                elif fetchall:
                    res = cur.fetchall()
                    return res

            except Exception as e:
                logger.error(f'An exception occurred: {e}')
                self.connection_close()

    def copy_file(self, query, filename):
        """ Метод для копирования данных из csv-файла в таблицу БД
        :param query: SQL-запрос
        :param filename: название csv-файла
        """

        if not self.__connection:
            self.connection_with_db()

        with self.__connection.cursor() as cur:
            with open(file=filename, mode='r', encoding='UTF-8') as ff:
                cur.copy_expert(query, ff)


    def connection_close(self):
        """ Метод, закрывающий соединение с БД """

        if self.__connection:
            self.__connection.close()
            logger.debug('Database connection closed')
