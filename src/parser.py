from time import time, sleep  # Функции для получения текущего времени в секундах и для задержки программы
import logging  # Библиотека для логгирования
from database import DB  # Класс БД
import consts  # Импорт модуля с константами
import config  # Импорт модуля с настройками подключения к БД
import queries  # Импорт модуля с запросами к БД
import requests  # Импорт библиотеки для работы с сетью

# Установка параметров логгирования
logging.basicConfig(
	format='%(asctime)s %(levelname)-8s %(name)s - %(message)s',
	level=logging.DEBUG,
	datefmt='%Y-%m-%d %H:%M:%S'
)

db = DB(
	host=config.HOST,
	# host='127.0.0.1',
	port=config.DB_PORT,
	# port=5432,
	user=config.USERNAME,
	# user='postgres',
	password=config.PASSWORD,
	# password='postgreSQL123',
	db_name=config.DB_NAME
	# db_name='gtfs'
)

logger = logging.getLogger('parser')  # Название логгера


def parse_data(response_content):
	""" Функция, осуществляющая парсинг данных из ответа сервера

	 :param response_content: Ответ сервера (объект-генератор, по которому можно итерироваться в цикле)

	 """

	lst_obj = []  # Список кортежей с информацией о камерах для сохранения в БД

	logger.info('Начало парсинга данных')

	for index, line in enumerate(response_content):
		# Пропуск первой строки с названиями столбцов
		if index == 0 or not line:
			continue

		camera_info = line.split(',')  # Разбиение строки на отдельные элементы
		camera_number = int(camera_info[0])  # Номер камеры
		lat, lon = float(camera_info[2]), float(camera_info[1])  # Широта и долгота
		camera_type = consts.CATEGORY_CODES[camera_info[3]]  # Тип опастности
		speed = camera_info[4]  # Ограничение скорости
		dir_type = consts.DIR_TYPE[camera_info[5]]  # Направление действия камеры:
		direction = camera_info[6]  # Градус обзора камеры (между 0 и 359, 0 - Север, 90 - Восток, 180 - Юг, 270 - Запад)

		lst_obj.append((index, camera_number, lat, lon, camera_type, speed, dir_type, direction, lon, lat))

		if index % 1000 == 0:
			save_data_in_db(lst_obj)
			lst_obj.clear()

	if lst_obj:
		save_data_in_db(lst_obj)

	logger.info(f'Конец парсинга данных')


def save_data_in_db(data):
	""" Функция, сохраняющая данные в БД

	 :param data: Данные в виде списка кортежей

	 """

	db.query_execute(queries.insert_update_table, params=data, ext=True)


def send_curl() -> requests.Response:
	""" Функция, отправляющая curl-запрос для получения информации о камерах

		:return Response: Ответ сервера
	 """

	response = requests.post(consts.URL, headers=consts.HEADERS, cookies=consts.COOKIES, json=consts.DATA)
	if response.status_code == 200:
		return response


def main():
	""" Точка входа """

	db.query_execute(queries.create_table)  # Создание таблицы БД, если она не создана

	resp = send_curl()  # Отправка curl-запроса

	if resp:
		resp_content = resp.iter_lines(delimiter='\n', decode_unicode=True) # Получение объекта-генератора строк
		parse_data(resp_content)  # Парсинг данных о камерах с сохранением их в БД
	else:
		logger.error(f'Не удалось послать curl-запрос на сайт {consts.URL}')


if __name__ == '__main__':
	db.connection_with_db()

	while 1:
		try:
			logger.info('Начало работы скрипта')
			start = time()
			main()
			logger.info(f'Скрипт работал: {round(time() - start, 2)} секунд')
			sleep(consts.INTERVAL_TO_PARSE)  # Задержка выполнения программы на указанное время (с)
		except BaseException as e:
			logger.error(f'Exeption occurred: {e}')
			db.connection_close()
			break
