from os import getenv

# Константы для подключения к БД
HOST = getenv('POSTGRES_HOST', 'postgres')
USERNAME = getenv('POSTGRES_USER', 'starlinemaps')
PASSWORD = getenv('POSTGRES_PASSWORD', 'starlinemaps')
DB_NAME = getenv('POSTGRES_DB', 'starline_speedcam')
DB_PORT = getenv('POSTGRES_PORT', 5432)

# Название таблицы
TABLE_NAME = getenv('TABLE_NAME')
