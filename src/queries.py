from config import TABLE_NAME

create_table = f""" CREATE TABLE IF NOT EXISTS {TABLE_NAME}(
                                                            id INTEGER PRIMARY KEY, 
                                                            camera_number INTEGER,
                                                            lat REAL, 
                                                            lon REAL,
                                                            danger_type VARCHAR(45),
                                                            speed INTEGER,
                                                            dir_type VARCHAR(53),
                                                            direction_degree INTEGER,
                                                            geometry GEOMETRY
                                                        ); """

insert_update_table = f""" INSERT INTO {TABLE_NAME} VALUES(%s, %s, %s, %s, %s, %s, %s, %s, ST_SetSRID(ST_MakePoint(%s, %s), 4326)) 
					  ON CONFLICT(id) DO UPDATE SET camera_number=EXCLUDED.camera_number, lat=EXCLUDED.lat, lon=EXCLUDED.lon,
                      danger_type=EXCLUDED.danger_type, speed=EXCLUDED.speed, dir_type=EXCLUDED.dir_type,
                      direction_degree=EXCLUDED.direction_degree, geometry=EXCLUDED.geometry; """


